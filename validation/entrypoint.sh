#!/bin/sh
## (We use sh since Alpine does not have Bash by default)


## exit immediately on commands with a non-zero exit status.
set -euo pipefail

## When started without any arguments, "-h", "--help", "-help" or "help", print
## usage.
if [ $# -eq 0 ] || [ x$1 == x"-h" ] || [ x$1 == x"--help" ] ||
   [ x$1 == x"-help" ] || [ x$1 == x"help" ]; then
    echo "    mmCIF file format validation tool."
    echo "------------------------------------------"
    echo "Provided by SWISS-MODEL / Schwede group"
    echo "(swissmodel.expasy.org / schwedelab.org)"
    echo ""
    echo "This container checks that mmCIF files are"
    echo "properly formatted according to the"
    echo "MAX/ mmCIF dictionary. At the moment,"
    echo "there is one tool available that acts as a"
    echo "command line tool: validate-mmcif-file."
    echo "For further usage information, call this"
    echo "container executing"
    echo "'validate-mmcif-file --help'."
    exit 1
fi

exec "$@"

#  LocalWords:  euo pipefail eq Schwede schwedelab mmcif fi
