# Test files for the validation tool

This is just a collection of files used by the [test suite](../test-suite.py) to the validation tool. File names should hint what is tested by a file.

Since we did not create (all) test cases from scratch but based them on existing files, here is a list of their whereabouts:

|Name         |Origin|License|
|-------------|----------------------------------------------------------------|
|working.\[cif\|zip\]|||
|missing_associated_file.\[cif\|zip\]|[ma-ornl-sphdiv-00211](https://www.modelarchive.org/doi/10.5452/ma-ornl-sphdiv-00211)|[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)|
|duplicated_items.cif|[ma-ornl-sphdiv-00211](https://www.modelarchive.org/doi/10.5452/ma-ornl-sphdiv-00211)|[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)|
