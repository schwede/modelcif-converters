#! /usr/local/bin/python
"""Get version and location of relevant mmCIF dictionaries for ModelCIF.

Fetch the versions of the ModelCIF dictionary and the PDBx/mmCIF dictionary used
to build it into a JSON file.
"""
# pylint: disable=invalid-name
# pylint: enable=invalid-name

import argparse
import sys

import rapidjson as json

from mmcif.io.IoAdapterPy import IoAdapterPy


def _parse_command_line():
    """Get arguments."""
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        "dic_file",
        type=str,
        metavar="<DICTIONARY FILE>",
        help="The mmCIF dictionary file to read the versions from.",
    )
    parser.add_argument(
        "--output",
        "-o",
        type=str,
        metavar="<PATH TO VERSION FILE>",
        help="Path to store the JSON file with the version at.",
        default="mmcif_ma_version.json",
    )
    parser.add_argument(
        "--child-location",
        "-l",
        type=str,
        metavar="<URL OF CHILD DICT FILE>",
        help="Download location of the child dictionary file.",
        default=None,
    )
    opts = parser.parse_args()

    return opts


def _error(msg):
    """Print a final error message."""
    print(msg + "\nAborting.", file=sys.stderr)
    sys.exit(1)


def _get_data_cat(cat, file_name, data):
    """Get a data category from a mmCIF data blob."""
    obj = data.getObj(cat)
    if obj is None:
        _error(f"No '{cat}' object found in '{file_name}'.")

    return obj


def _get_data_item(itm, cat, file_name, cat_data):
    """Get a single data item from a data category."""
    val = cat_data.getAttributeValueList(itm)
    if len(val) != 1:
        _error(
            f"Expected exactly 1 '{cat}.{itm}' in '{file_name}', "
            + f"found '{', '.join(val)}'."
        )

    return val[0]


def _get_versions(dic_file, io_adapter):
    """Fetch the 'category_group_list' object and assemble a version for the
    dictionary."""

    dic = io_adapter.readFile(inputFilePath=dic_file)

    # fetch a data container from the list returned by the adapter
    cntnr = None
    for obj in dic:
        if "dictionary" in obj.getObjNameList():
            cntnr = obj
            break

    if cntnr is None:
        _error(f"No 'dictionary' object found in '{dic_file}'.")

    dic = _get_data_cat("dictionary", dic_file, cntnr)

    vrsn = _get_data_item("version", "dictionary", dic_file, dic)
    ttl = _get_data_item("title", "dictionary", dic_file, dic)
    dic_version = {"title": ttl, "version": vrsn}

    return dic_version


def _add_dict_location(child, child_loc):
    """Add URLs to the dictionary versions if available."""
    if child_loc is None:
        child["location"] = "."
    else:
        child["location"] = child_loc


def _main():
    """Run as script."""
    opts = _parse_command_line()

    io_adapter = IoAdapterPy(False, sys.stdout)
    c_vrsn = _get_versions(opts.dic_file, io_adapter)

    _add_dict_location(c_vrsn, opts.child_location)
    with open(opts.output, "w", encoding="utf8") as jfh:
        json.dump({"versions": [c_vrsn]}, jfh)


if __name__ == "__main__":
    _main()

#  LocalWords:  DictToSdb SDB PDBx CifCheck pylint mmcif pdbx dic nAborting
#  LocalWords:  macromolecular utf
