# modelcif-converters

This is a collection of tools, code and examples used for converting user's modelling projects into [ModelCIF format](https://mmcif.wwpdb.org/dictionaries/mmcif_ma.dic/Index/) for upload to [ModelArchive](https://www.modelarchive.org) (MA). The whole repository has grown over time from bulk uploads to MA, we assisted users with. Do not expect everything to work out of the box, old conversion projects may be outdated by now.

If you have questions and/ or need assistance in converting your models to ModelCIF, feel free to contact the [MA team](https://modelarchive.org/contact).

## ModelCIF resources

The official documentation of [ModelCIF](https://mmcif.wwpdb.org/dictionaries/mmcif_ma.dic/Index/) and [PDBx/mmCIF](https://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v50.dic/Index/) can be found here: https://mmcif.wwpdb.org

ModelCIF dictionary files can be found in the official repository: https://github.com/ihmwg/ModelCIF


## Directories

A short overview of the directories in this repository and what they do.

|Path       |Content                                                         |
|-----------|----------------------------------------------------------------|
|[projects/](projects/)|Collection of model conversions done for various user projects. |
|[projects/docker](projects/docker)|Docker setup to run the conversion software|
|[validation/](validation/)|A tool to check the formatting of ModelCIF files.               |

<!--  LocalWords:  modelcif ModelArchive PDBx
 -->
