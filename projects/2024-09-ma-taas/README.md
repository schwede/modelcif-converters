# Modelling of full-length trimeric autotransporter adhesins (TAAs)

[Link to project in ModelArchive](https://www.modelarchive.org/doi/10.5452/ma-taas) (incl. background on project itself)

Modelling setup:
- Step 1: AlphaFold-Multimer (AF) using v2.1.2 or v2.3.1 with different parameters for overlapping ranges of full-length sequence
- Step 2 (if needed): AF-models truncated and superposed and assembled using MODELLER
- Step 3 (if multiple models combined): custom QE calculated combining pLDDT and MODELLER energy score
- Step 4 (for assemblies): domain annotation performed using a combination of DSSP, SamCC-Turbo, Foldseek, and US-align
- Single domain models only included step 1 and a truncation step (usually a subset of another model in the same set)
- Full-length sequences taken from NCBI.

Input files for conversion:
- Single ZIP file for each model
- Each ZIP file contains:
  - assembly_info.json or domain_info.json with metadata on model
  - assembly.cif or domain.cif with the model coordinates
  - accompanying_data.zip or accompanying_data sub-folder with additional files as listed in the metadata
  - image.png with an overview image for use in ModelArchive

Special features here compared to past projects:
- NCBI data loaded in bulk before doing conversion (helpful to process large set in parallel).
- Generic `ZipFileHandler` class to simplify processing ZIP files.
- Fills `not_modeled_residue_ranges` which was recently added to python-modelcif.
- Enable packing of associated files without having to dump data to disk (avoids having to extract files from provided ZIP files).
- Used software includes AlphaFold-Multimer, MODELLER, DSSP, Foldseek, SamCC-Turbo, and US-align.
- QE metrics linked to software used (fetched from protocol steps to avoid duplicated entries in ModelCIF).
- Complex modelling pipeline with different variants of the same software (AF) being used to produce distinct intermediate files. Global definition of objects was needed to avoid duplicated entries in ModelCIF. Data input/output for protocol steps handled with lists of keywords.
- Provided assembly.cif files contain additional mmCIF data to keep (categories pdbx_domain, pdbx_feature_domain, pdbx_domain_range kept and added using gemmi).
- Intermediate models kept as accompanying data in PDB format. AF-models additionally with scores in JSON format (processed from pickle files into more portable JSON) and PAE matrix as PNG file.
- Use of modelcif.reference.SeqDif to handle mismatches between NCBI and entity sequence (simplified compared to 2024-08-ma-dm-hisrep).
- Similar checking for issues as in 2024-04-ma-dm-prc.

Content:
- translate2modelcif.py : script to do conversion (run in virtual environment with same setup as Docker container here; using OST 2.8, python-modelcif 1.1 and python-ihm 1.6)
- fetch_ncbi_entries.py : script used to obtain info from NCBI stored in ncbi_data.json
- ncbi_data.json.gz : version of ncbi_data.json used for this conversion (from 24.9.2024); to be unpacked to use translate2modelcif.py
- ma-taas-0272.zip : example for a small assembly in the set
- ma-taas-9037.zip : example for a small domain in the set
- example_modelcif.zip : output from running conversion of examples with the commands below:
```
ost translate2modelcif.py ma-taas-0272.zip modelcif
ost translate2modelcif.py ma-taas-9037.zip modelcif
```
