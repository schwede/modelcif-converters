# Modelling of histone complexes (structural prediction screen)

[Link to project in ModelArchive](https://www.modelarchive.org/doi/10.5452/ma-dm-hisrep) (incl. background on project itself)

Input files for conversion:
- Annotations.json with metadata (incl. UniProtKB AC)
- Config_Files directory with config_[X].json files and dates.json with model creation dates
- Zip_Files directory with files named [X]-[NAME].zip for each X listed in the metadata files
- ZIP files are expected to contain the 3 top ranked models (.pdb) with their respective scores (.json) and .png files for coverage-, pLDDT-, and PAE-plots as produced by ColabFold (all with their original file names)

Modelling setup:
- Classic ColabFold setup with PDB coordinates and scores JSON and link to UniProt

Special features here compared to the PRC-complexes script and PP2A-B55-design project:
- UniProt link: handling of subsets of sequences and with old versions and fixed cache of UniProt data, code adapted from PP2A-B55-design project
- Use of modelcif.reference.SeqDif to handle mismatches between UniProt and entity sequence, improved from PP2A-B55-design to use more of the alignment data (alignment start/end)
- Improved of PP2A-B55-design caching mechanism, with model sequence specific keys, when fetching UniProt data
- Processing of the model creation dates in dates.json, intentionally provided to determine ColabFold database names and versions, with the _get_cf_db_versions function
- Renaming of chain labels in the .pdb that have been shifted compared to the metadata ('A'->'B', 'B'->'C', etc.)
- Special processing for the model 263, to ignore the 5 first aa introduced in the model sequence during the sequence alignment algorithm
- local plddt accuracy threshold doubled to 0.011
- iptm score check ignored

Content:
- translate2modelcif.py : script to do conversion (run in virtual environment with same setup as Docker container here but with OST 2.8 and very latest main branch of python-modelcif and python-ihm from 20.6.2024)
- minimal_example.zip: example input to convert a single complex from this set
- minimal_example_modelcif: output from running conversion of minimal_example with the command bellow : 

```python3 translate2modelcif.py ./ModelArchive-ma-dm-hisrep ./modelcif --single-model 3 --no-extra-files```