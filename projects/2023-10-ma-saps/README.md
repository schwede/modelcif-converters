# Modelling of phytoplasma effectors

[Link to project in ModelArchive](https://modelarchive.org/doi/10.5452/ma-saps) (incl. background on project itself)

Setup:
- Using AlphaFold v2.2 for monomer predictions with fairly default settings
- Conversion based only on PDB files without extra data
- CSV file with links to accession codes in UniProt

Special features here:
- Generic code added to find best sequence match in UniProt history and report mismatches
- Sequences with remaining mismatches (here due to experimental assays) labelled in entity name
- One obsolete entry (SAP11_MBS linked to A0A1B3JKP4 in UniProt or UPI00083DE1DE in UniParc)
- pLDDT extracted from b-factors (simplest setup since no other QA scores anyway)

Content:
- translate2modelcif.py : script to do conversion
- input_data: PDB files and CSV used for conversion
