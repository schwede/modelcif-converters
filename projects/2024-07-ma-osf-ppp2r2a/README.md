# Modelling of PP2A-B55-design

[Link to project in ModelArchive](https://www.modelarchive.org/doi/10.5452/ma-osf-ppp2r2a) (incl. background on project itself)

Input files for conversion:
- info_of_submitted_structures.csv with metadata (incl. per chain mapping to entity description, subsetting, UniProtKB AC, iPlddt and iPAE scores, binding experiment results )
_ 4 models directories : screen_256, holoenzyme, relaxes_figures and design
- config.json in every directory above
- [NAME].pdb for each model NAME listed in info_of_submitted_structures.csv, in the directory indicated in the metadata
- [NAME].json their respective scores for every model except the relaxed models containg the string 'only_pep' in their NAME

Modelling setup:
- Classic ColabFold setup with PDB coordinates and scores JSON and link to UniProt
- Two custom model quality scores (ipLDDT, iPAE)
- Experimental validation of binding and only validated or known interactions exported to 3D-Beacons
- Four different modelling setups (PPI screening, relaxed models of selected screening results, full holoenzyme complex, design of protein-peptide complex)

Special features here compared to PRC-complexes script:
- UniProt-link: deal with subsets of sequences and with old versions and fixed cache of UniProt data (keyed also on entity sequence)
- Use of modelcif.reference.SeqDif to deal with mismatches between UniProt and entity sequence
- Deal with synthetic constructs (design) which do not have a link to UniProt
- Custom cut of JSON scores for PAE and pLDDT for some relaxed models
- Updated _get_cf_config function to deal with null values in ColabFold's config.json and to reset tpl_db and tpl_db_version if no templates used

Content:
- translate2modelcif.py : script to do conversion (run in virtual environment with same setup as Docker container here but with OST 2.8 and very latest main branch of python-modelcif and python-ihm from 20.6.2024)
- modelarchive_submission.zip: example inputs to convert selected complex from this set, compressed
- expected_output_modelcif.zip: compressed output from running conversion of modelarchive_submission with the following command :
`python3  translate2modelcif.py ./modelarchive_submission ./modelcif  --no-extra-files`

