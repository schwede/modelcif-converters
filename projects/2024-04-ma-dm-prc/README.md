# Modelling of PRC1 and 2 complexes (structural prediction screen)

[Link to project in ModelArchive](https://modelarchive.org/doi/10.5452/ma-dm-prc) (incl. background on project itself)

Input files for conversion:
- Annotations.csv and Annotations.json with metadata (incl. per chain mapping to entity description and UniProtKB AC)
- Config_Files directory with config_[X].json files for all X listed in Config column of Annotations.csv
- Zip_Files directory with files named [X]-[NAME].zip for each X listed in the metadata files
- ZIP files are expected to contain the 3 top ranked models (.pdb) with their respective scores (.json) and .png files for coverage-, pLDDT-, and PAE-plots as produced by ColabFold 1.5.2 (all with their original file names)

Modelling setup:
- Screening exercise to identify protein pairs which may interact
- 742 structural predictions deposited; 740 dimers (homomers and heteromers) and 2 larger complexes
- All done with ColabFold 1.5.2 using 2 different setups (distinguished by 2 config.json files created by ColabFold)
- Sequences all taken from UniProtKB

Special features here:
- Using ColabFold data directly from zipped project folders (convenient to reduce data to exchange with depositors)
- UniProtKB entry parser which can work both with remote URLs and local TXT files
- Generic sequence aligner which can fill gaps for non-default AA (i.e. "XOUBJZ") using reference sequence and returns covered ranges, sequence mismatches and alignment (with option to do aln. to atom seq.). Note: here we cannot use auto-fill of gaps due to pLDDT and PAE scores parsing.
- Entity generation automatically groups chains for homomers
- Automatic processing of ColabFold setup using config.json and DB versions provided by user (valid until v1.5.5 up to March 2024)
- Compatible with recently added python-modelcif changes of adding SW parameters per SW (instead of per SW group) and writing "struct_ref" in addition to "ma_target_ref_db_details" (here restricted to full models being covered by reference seq. without mismatches)
- Extended LPeptideAlphabet which can handle all 26 one-letter-codes
- Accompanying data includes PAE, extra models (described with global scores), and PNG files for ColabFold plots
- System to save identified issues in a JSON file (here distinguishing minor and major mismatches between modelled and reference sequence and checking for score mismatches between JSON file and b-factors in PDB file and scores in Annotations.csv)
- Saves entries to be exported to 3D-Beacons in separate JSON file (most interactions were only meant for checking and are not expected to be real interactions; only models for known interactions exported to 3D-Beacons)

Content:
- translate2modelcif.py : script to do conversion (run in virtual environment with same setup as Docker container here but with OST 2.7 and very latest main branch of python-modelcif and python-ihm from 23.4.2024)
- minimal_example: example input to convert a single complex from this set
- minimal_example_modelcif: output from running conversion of minimal_example
