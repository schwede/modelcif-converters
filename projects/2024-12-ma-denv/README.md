# Modelling AVITHRAID

[Link to project in ModelArchive](https://www.modelarchive.org/doi/10.5452/ma-denv) (incl. background on project itself and link to the publication)

Modelling setup:
- SWISS-MODEL web server
- all jobs ran in auto-mode

Input files for conversion:
- SWISS-MODEL ModelCIF files

Special features here:
- None, just plain SWISS-MODEL ModelCIF files

Content:
- translate2modelcif.py: Script to enrich ModelCIF files
- uniprotkb.py: Module to deal with UNP entries
- macif.py: Module to aid editing ModelCIF files
- example*: Example files
