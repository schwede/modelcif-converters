# -*- coding: utf-8 -*-

"""Check & extend SWISS-MODEL models for ma-denv

Example for running:
python translate2modelcif.py --unp-json example_unp_data.json \
                             example_swissmodelcif.cif.gz \
                             example_index.csv \
                             done

This call uses the example data from this directory. You may need to decompress
some of the files. The resulting './done/example_swissmodelcif.cif.gz' should
be identical to './example_swissmodelcif.cif.gz'.

Expected output in ./done for example above:
- example_swissmodelcif.cif.gz as ModelCIF file with extended annotation
"""

from datetime import date
import argparse
import csv
import os
import sys

from macif import MABlock
from uniprotkb import UniProtKBEntryCache


################################################################################
# GENERAL HELPER FUNCTIONS
################################################################################
def _abort_msg(msg, exit_code=1):
    """Write error message and exit with exit_code."""
    print(f"{msg}\nAborting.", file=sys.stderr)
    sys.exit(exit_code)


def _check_file(file_path):
    """Make sure a file exists and is actually a file."""
    if not os.path.exists(file_path):
        _abort_msg(f"File not found: '{file_path}'.")
    if not os.path.isfile(file_path):
        _abort_msg(f"File path does not point to file: '{file_path}'.")


def _check_folder(dir_path, create):
    """Make sure a file exists and is actually a file."""
    if not os.path.exists(dir_path):
        if not create:
            _abort_msg(f"Path not found: '{dir_path}'.")
        os.makedirs(dir_path, exist_ok=True)
    if not os.path.isdir(dir_path):
        _abort_msg(f"Path does not point to a directory: '{dir_path}'.")


def _check_opts_folder(dir_path, create=False):
    """Remove trailing '/' (return fixed one) and check if path valid."""
    if dir_path.endswith("/"):
        dir_path = dir_path[:-1]
    _check_folder(dir_path, create=create)
    return dir_path


################################################################################


################################################################################
# DATA HANDLING
################################################################################
def _parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__,
    )

    parser.add_argument(
        "modelcif",
        type=str,
        metavar="<INPUT MODELCIF FILE>",
        help="Path to a SWISS-MODEL ModelCIF file provided by depositors.",
    )
    parser.add_argument(
        "model_info_csv",
        type=str,
        metavar="<MODEL INFO CSV FILE>",
        help="Path to a CSV file with information about each model. Format: "
        + "<FILENAME>,<UNP AC>,<UNP ALIGNMENT START>,<UNP ALIGNMENT END>,"
        + "<ENTITY DESCRIPTION>,<MODEL TITLE>,<MODEL DESCRIPTION>",
    )
    parser.add_argument(
        "out_dir",
        type=str,
        metavar="<OUTPUT DIR>",
        help="Path to directory to store results. The updated ModelCIF file "
        + "will be stored in this directory. If the ModelCIF file already "
        + "exists in the output directory, it will not be overwritten and an "
        + "error thrown.",
    )
    parser.add_argument(
        "--unp-json",
        type=str,
        default="./unp_data.json",
        help="UniProtKB entries data. Information for each UNP entry fetched "
        + "will be stored here. Serves as a cache. To update (e.g. after an "
        + "UNP release), just delete the file and it will be recreated.",
    )
    parser.add_argument(
        "--compress",
        "-c",
        default=False,
        action="store_true",
        help="Compress ModelCIF file with gzip.",
    )
    parser.add_argument(
        "--checks-only",
        default=False,
        action="store_true",
        help="Only check for issues without producing ModelCIF files.",
    )
    opts = parser.parse_args()

    # check input
    _check_file(opts.modelcif)
    _check_file(opts.model_info_csv)
    _check_opts_folder(opts.out_dir, create=True)
    # check if the ModelCIF file already exists in out_dir
    out_file = os.path.join(
        opts.out_dir,
        f"{os.path.basename(opts.modelcif)}{'.gz' if opts.compress else ''}",
    )
    if os.path.exists(out_file):
        _abort_msg(f"'{out_file}' already exists, will *not* be overwritten.")
    # check if UNP is a file if it already exists
    if os.path.exists(opts.unp_json):
        _check_file(opts.unp_json)

    return opts


def _read_csv(mdl_file, mdl_info_csv):
    """Get info of a specific model from CSV file"""
    mdl_file = os.path.basename(mdl_file)

    mdl_info = {}
    with open(mdl_info_csv, newline="", encoding="ascii") as csvh:
        info_r = csv.reader(csvh)
        for row in info_r:
            assert len(row) == 7
            if row[0].endswith(mdl_file):
                # <UNP AC>
                mdl_info["unp_ac"] = row[1]
                # <UNP ALIGNMENT START>
                mdl_info["unp_start"] = int(row[2])
                # <UNP ALIGNMENT END>
                mdl_info["unp_end"] = int(row[3])
                # ?(IS THIS A UNP FIELD? MAYBE CODE?)
                mdl_info["protein_desc"] = row[4]
                # <MODEL TITLE>
                mdl_info["entry_title"] = row[5]
                # <MODEL DESCRIPTION>
                mdl_info["entry_desc"] = row[6]
                break
    if len(mdl_info) == 0:
        _abort_msg(f"Could not find '{mdl_file}' in '{mdl_info_csv}'.")

    return mdl_info


################################################################################


################################################################################
# HANDLE FULL DATA SET
################################################################################
def _update_modelcif(mdl_file, mdl_info, unp_json_file, out_dir, compress):
    """Update ModelCIF file with model info and verify UNP related data.
    Caution: This is for updates BEFORE deposition, this update does not do
    a CIF-style revision history for you!"""

    block = MABlock(mdl_file)
    assert len(block.polymer_targets) == 1
    target = block.polymer_targets[0]
    unp = UniProtKBEntryCache(unp_json_file)
    # print(mdl_info)
    unp, db_aln, seq_aln = unp.match_sequence(
        mdl_info["unp_ac"],
        target["sequence"],
        mdl_info["unp_start"],
        mdl_info["unp_end"],
    )

    # update ModelCIF data
    block.add_to_category(
        "struct",
        title=mdl_info["entry_title"],
        pdbx_model_details=mdl_info["entry_desc"],
    )
    block.add_to_category(
        "entity",
        pdbx_description=mdl_info["protein_desc"],
        match=("id", target["entity_id"]),
    )
    block.add_to_category(
        "entity",
        src_method="nat",
        match=("id", target["entity_id"]),
    )
    block.add_category(
        "audit_author",
        name=[
            "Guarnetti Prandi, Ingrid",
            "Chillemi, Giovanni",
            "Talarico, Carmine",
        ],
        pdbx_ordinal=[1, 2, 3],
        after="ma_software_group",
    )
    # Update ModelCIF files with UniProtKB info
    struct_ref_id = "1"
    block.add_category(
        "struct_ref",
        id=[struct_ref_id],
        entity_id=[target["entity_id"]],
        db_name=["UNP"],
        db_code=[unp.unp_id],
        pdbx_db_accession=[unp.unp_ac],
        pdbx_align_begin=[mdl_info["unp_start"]],
        pdbx_seq_one_letter_code=[unp.unp_seq],
        details=[unp.unp_details_full],
        after="entity",
    )
    block.add_category(
        "struct_ref_seq",
        align_id=["1"],
        ref_id=[struct_ref_id],
        seq_align_beg=[seq_aln[0]],
        seq_align_end=[seq_aln[1]],
        db_align_beg=[db_aln[0]],
        db_align_end=[db_aln[1]],
        after="struct_ref",
    )
    block.add_category(
        "ma_target_ref_db_details",
        target_entity_id=[target["entity_id"]],
        db_name=["UNP"],
        db_name_other_details=[False],
        db_code=[unp.unp_id],
        db_accession=[unp.unp_ac],
        seq_db_isoform=[False],
        seq_db_align_begin=[db_aln[0]],
        seq_db_align_end=[db_aln[1]],
        ncbi_taxonomy_id=[unp.ncbi_taxid],
        organism_scientific=[unp.organism_species],
        seq_db_sequence_version_date=[date.isoformat(unp.last_seq_change)],
        seq_db_sequence_checksum=[unp.unp_crc64],
        after="struct_ref_seq",
    )
    # write, if requested write compressed
    if out_dir is not None:
        block.write_file(
            os.path.join(out_dir, os.path.basename(mdl_file)), compress
        )


################################################################################


def _main():
    """Run as script."""
    opts = _parse_args()

    # read model info from CSV
    mdl_info = _read_csv(opts.modelcif, opts.model_info_csv)
    print(f"Working on model {opts.modelcif}...")
    _update_modelcif(
        opts.modelcif,
        mdl_info,
        opts.unp_json,
        opts.out_dir if not opts.checks_only else None,
        opts.compress,
    )
    print(f"... done with model {opts.modelcif}.")

    # TEST: to judge res. needed on cluster
    # import resource
    # print("mem", resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1000)


if __name__ == "__main__":
    _main()

#  LocalWords:  gemmi
