"""Functions to retrieve data for UniProtKB entries.
"""

# Copyright (c) 2023, SIB - Swiss Institute of Bioinformatics and
#                     Biozentrum - University of Basel
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import datetime
import os
import requests

try:
    # pylint: disable="bare-except"
    import ujson as json
except:
    import json

import parasail

# Name of UniProtKB as _ma_target_ref_db_details.db_name in a ModelCIF file,
# but 'pythonified' (no leading '_', no '.').
MA_TARGET_REF_DB_DETAILS_DB_NAME = "UNP"


def translate_upkb_date_string(date_string):
    """UniProtKB uses 3-letter month codes, which do not fly in other languages
    (locales) than English, e.g. if the locale is set to de_DE, strptime()
    expects 'MAI' instead of 'MAY'... switching the locale temporarily is also
    not so easy (threads, not setting it back upon exceptions...). Hence, we
    make the month a numeric value, here.
    """
    for i, mon in enumerate(
        [
            "JAN",
            "FEB",
            "MAR",
            "APR",
            "MAY",
            "JUN",
            "JUL",
            "AUG",
            "SEP",
            "OCT",
            "NOV",
            "DEC",
        ],
        start=1,
    ):
        if mon in date_string:
            return date_string.replace(mon, f"{i:02}")

    raise RuntimeError(
        "Unrecognised UniProtKB date string found: " + f"'{date_string}'."
    )


class UniProtKBEntry:
    """Deal with UniProtKB entries."""

    # We ignore PEP8 on the number of attributes per class, here. The objective
    # of UniProtKBEntry is to... represent a UniProtKB entry with all its
    # data/ attributes. By the nature of an entry's meta-data, breaking it up
    # into sub-classes seems counter-intuitive.
    # Since this is more a data class than a functional class, we allow having
    # too few public methods for now.
    # pylint: disable=too-many-instance-attributes, too-few-public-methods

    def __init__(self, unp_ac, entry_version=None, json_data=None):
        """Create a new UniProtKB entry object.

        UniProtKB API will be queried immediately on creation.

        :param unp_ac: Accession code of the UniProtKB entry to be fetched.
        :type: unp_ac: :class:`str`
        :param entry_version: Version of the UniPrtoKB entry to be fetched (not
                              to be mixed up with the UniProtKB release).
        :type entry_version: :class:`str` or :class:`int`
        :param json_data: Init object from JSON object, ignores unp_ac.
        :type json_data: :class:`dict`
        """
        if json_data is None:
            self.unp_ac = unp_ac
            # filled by self._fetch()
            self.entry_status = None
            self.entry_version = (
                int(entry_version) if entry_version is not None else None
            )
            self.first_appearance = None
            self.last_change = None
            self.last_seq_change = None
            self.ncbi_taxid = None
            self.organism_species = ""
            self.seq_version = None
            self.seqlen = None
            self.unp_crc64 = None
            self.unp_details_full = None
            self.unp_id = None
            self.unp_seq = ""
            self._fetch()
            assert len(self.unp_seq) == self.seqlen
        else:
            self.entry_status = json_data["entry_status"]
            self.entry_version = int(json_data["entry_version"])
            self.first_appearance = (
                datetime.fromisoformat(json_data["first_appearance"])
                if json_data["first_appearance"] is not None
                else None
            )
            self.last_change = (
                datetime.fromisoformat(json_data["last_change"])
                if json_data["last_change"] is not None
                else None
            )
            self.last_seq_change = (
                datetime.fromisoformat(json_data["last_seq_change"])
                if json_data["last_seq_change"] is not None
                else None
            )
            self.ncbi_taxid = json_data["ncbi_taxid"]
            self.organism_species = json_data["organism_species"]
            self.seq_version = json_data["seq_version"]
            self.seqlen = json_data["seqlen"]
            self.unp_ac = json_data["ac"]
            self.unp_crc64 = json_data["crc64"]
            self.unp_details_full = json_data["details_full"]
            self.unp_id = json_data["id"]
            self.unp_seq = json_data["seq"]

    def __str__(self):
        """Print object as string."""
        return (
            f"<{__name__}.{type(self).__name__} AC={self.unp_ac} "
            + f"version={self.entry_version}>"
        )

    def _parse_id_line(self, line):
        """Parse a UniProtKB TXT format's ID line.

        Should support some older format versions, too."""
        sline = line.split()
        if len(sline) != 5:
            # Some old formats that are easy to fix
            if len(sline) == 6 and sline[3] == "PRT;":
                sline.pop(3)
            else:
                raise RuntimeError(
                    "ID line not conforming to 'ID   EntryName"
                    + f"Status; SequenceLength.', found: {line}"
                )
        self.unp_id = sline[1]
        self.entry_status = sline[2][:-1].upper()
        self.seqlen = int(sline[3])

    def _parse_dt_line(self, line):
        """Parse a UniProtKB TXT format's DT line.

        Should support some older format versions, too."""
        sline = line.split()
        sline[1] = translate_upkb_date_string(sline[1])
        if sline[2] == "(Rel.":  # old format
            if sline[4] == "Created)":
                self.first_appearance = datetime.strptime(sline[1], "%d-%m-%Y")
                self.entry_version = int(sline[3][:-1])
            elif sline[5] == "sequence":
                self.last_seq_change = datetime.strptime(sline[1], "%d-%m-%Y")
                self.seq_version = int(sline[3][:-1])
            elif sline[5] == "annotation":
                self.last_change = datetime.strptime(sline[1], "%d-%m-%Y")
            return
        if sline[2] == "integrated":
            self.first_appearance = datetime.strptime(sline[1], "%d-%m-%Y,")
        elif sline[2] == "sequence":
            self.last_seq_change = datetime.strptime(sline[1], "%d-%m-%Y,")
            self.seq_version = int(sline[4][:-1])
        elif sline[2] == "entry":
            self.last_change = datetime.strptime(sline[1], "%d-%m-%Y,")
            self.entry_version = int(sline[4][:-1])

    def _parse_de_line(self, line):
        """Parse a UniProtKB TXT format's DE line(s)."""
        sline = line.split()
        if sline[1] == "RecName:":
            if self.unp_details_full is None:
                if sline[2].startswith("Full="):
                    self.unp_details_full = sline[2][len("Full=") :]
                for i in sline[3:]:
                    if i.startswith("{"):
                        break
                    self.unp_details_full += f" {i}"
                    if self.unp_details_full.endswith(";"):
                        self.unp_details_full = self.unp_details_full[:-1]
                        break

    def _parse_os_line(self, line):
        """Parse a UniProtKB TXT format's OS line(s)."""
        osl = len("OS   ")
        if line[-1] == ".":
            self.organism_species += line[osl:-1]
        else:
            self.organism_species += line[osl:-1] + " "

    def _parse_ox_line(self, line):
        """Parse a UniProtKB TXT format's OX line."""
        # Taxonomy codes only come from NCBI atm.
        sline = line.split("=")
        self.ncbi_taxid = sline[-1][:-1]
        self.ncbi_taxid = self.ncbi_taxid.split()[0]

    def _parse_sq_line(self, line):
        """Parse a UniProtKB TXT format's SQ line."""
        sline = line.split()
        self.unp_crc64 = sline[6]

    def _parse_sequence(self, line):
        """Parse the sequence out of the UniProtKB TXT format."""
        sline = line.split()
        self.unp_seq += "".join(sline)

    def _fetch(self):
        """Retrieve information for a single UniProtKB entry."""
        if self.entry_version is None:
            query_url = f"https://rest.uniprot.org/uniprotkb/{self.unp_ac}.txt"
        else:
            query_url = (
                f"https://rest.uniprot.org/unisave/{self.unp_ac}?format=txt&"
                + f"versions={self.entry_version}"
            )

        rspns = requests.get(query_url, timeout=180)
        if not rspns.ok:
            raise RuntimeError(
                f"UniProtKB entry with AC '{self.unp_ac}' not retrieved for "
                + f"URL '{query_url}'"
            )
        for line in rspns.iter_lines(decode_unicode=True):
            # Check here to learn about UniProtKB's flat file format:
            # https://web.expasy.org/docs/userman.html
            # ID   EntryName Status; SequenceLength.
            if line.startswith("ID   "):
                self._parse_id_line(line)
            # DT   DD-MMM-YYYY, integrated into UniProtKB/database_name.
            # DT   DD-MMM-YYYY, sequence version x.
            # DT   DD-MMM-YYYY, entry version x.
            elif line.startswith("DT   "):
                self._parse_dt_line(line)
            # DE   RecName: Full=Highly reducing ... tstA {ECO:...|PubMed:...};
            # DE            Short=HR-PKS phiA {ECO:0000303|PubMed:26558485};
            # DE            EC=2.3.1.- {ECO:0000269|PubMed:26558485};
            # DE   AltName: Full=Phomoidride ... protein A {ECO:...|PubMed:...};
            # OS   Avian leukosis RSA (RSV-SRA) (Rous sarcoma virus (strain
            # OS   Schmidt-Ruppin A)).
            elif line.startswith("DE   "):
                self._parse_de_line(line)
            elif line.startswith("OS   "):
                self._parse_os_line(line)
            # OX   Taxonomy_database_Qualifier=Taxonomic code;
            elif line.startswith("OX   NCBI_TaxID="):
                self._parse_ox_line(line)
            # SQ   SEQUENCE   3392 AA;  378905 MW;  BBD894175578E164 CRC64;
            elif line.startswith("SQ   "):
                self._parse_sq_line(line)
            # Seqeunce is stored special, w/o prefix and multiline
            elif line.startswith("     "):
                self._parse_sequence(line)
            # print(line)

    def to_json(self):
        """Return entry as JSON object."""
        return {
            "ac": self.unp_ac,
            "entry_version": self.entry_version,
            "organism_species": self.organism_species,
            "entry_status": self.entry_status,
            "first_appearance": (
                datetime.isoformat(self.first_appearance)
                if self.first_appearance is not None
                else None
            ),
            "last_change": (
                datetime.isoformat(self.last_change)
                if self.last_change is not None
                else None
            ),
            "last_seq_change": (
                datetime.isoformat(self.last_seq_change)
                if self.last_seq_change is not None
                else None
            ),
            "ncbi_taxid": self.ncbi_taxid,
            "seq_version": self.seq_version,
            "seqlen": self.seqlen,
            "details_full": self.unp_details_full,
            "id": self.unp_id,
            "crc64": self.unp_crc64,
            "seq": self.unp_seq,
        }


class UniProtKBEntryCache:
    """Cached retrieval of UniProtKB entries.

    To avoid calling the UniProtKB API for the same UniProtKB AC multiple times,
    use this cache. Also helps with keeping code cleaner since you do not need
    to carry UniProtKBEntry instances around, just call the cached entry.

    Be careful about UniProtKB entry versions. In theory, when specifying no
    version for the cache, the real entry may change at UniProtKB while
    running... but this is also an issue when loading the entry live from
    UniProtKB multiple times.

    Also be aware that the cache has no size restrictions, it does not get swept
    over its whole lifetime."""

    # The cache serves as a data-dump, storing and handing out instances. No
    # need for a lot of public methods, so we ignore PEP, here.
    # pylint: disable=too-few-public-methods
    def __init__(self, json_cache_file=None):
        """Set up the cache."""
        self._cache = {}
        self._cache_file = json_cache_file

        # try to fill the cache from file
        if (
            self._cache_file is not None
            and os.path.exists(self._cache_file)
            and os.stat(self._cache_file).st_size != 0
        ):
            with open(self._cache_file, encoding="utf8") as jfh:
                self._cache = self._from_json(json.load(jfh))

    def get(self, unp_ac, entry_version=None):
        """Get an UniProtKBEntry from the cache.

        If the entry can not be found, it will be fetched from the UniProtKB
        API."""
        try:
            return self._cache[unp_ac][entry_version]
        except KeyError:
            unp = UniProtKBEntry(unp_ac, entry_version=entry_version)
            if unp_ac not in self._cache:
                self._cache[unp_ac] = {}
            self._cache[unp_ac][entry_version] = unp
            # if we end up here, store the cache on disk
            if self._cache_file is not None:
                with open(self._cache_file, "w", encoding="utf8") as jfh:
                    json.dump(self.to_json(), jfh)

        return self._cache[unp_ac][entry_version]

    def to_json(self):
        """Turn the cache into a JSON object."""
        data = {}
        for acc, versions in self._cache.items():
            data[acc] = {}
            for version, entry in versions.items():
                data[acc][version] = entry.to_json()

        return data

    def _from_json(self, data):
        """Used to initialise the cache from a JSON object."""
        cache = {}
        for acc, versions in data.items():
            cache[acc] = {}
            for version, entry in versions.items():
                version = int(version) if version != "null" else None
                cache[acc][version] = UniProtKBEntry(None, json_data=entry)

        return cache

    def match_sequence(self, unp_ac, sequence, start, end):
        """Match a sequence with the sequence of a UNP entry.

        As the UNP sequence can change over time, walk through the versions
        until exact match or return the best match.

        :param unp_ac: UniProtKB Accession.
        :type unp_ac: :class:`str`
        :param sequence: Target sequence of the model.
        :type sequence: :class:`str`
        :param start: Start residue of the alignment, 1-based.
        :type start: :class:`int`
        :param end: End residue of the alignment 1-based.
        :type end: :class:`int`
        """

        # This function was first written for a bulk deposition that came with
        # alignment range in the UNP sequence, UNP AC but no UNP entry version.
        # So this function works with this for now. Later use cases should be
        # incorporated as they occur. E.g. having no range (start, end would be
        # None) should search for the best alignment and use the range from
        # this in the ModelCIF file.

        def aln(unp, trg, start, end):
            unp = unp[start:end]
            # trg = parasail's query
            # unp = parasail's ref
            # Since we have a fixed range, we expect a global alignment, use
            # parasail's NW variant.
            alignment = parasail.nw_trace_scan_sat(
                trg, unp, 5, 2, parasail.blosum62
            )
            # get aln boundaries - assuming a NW (global) alignment
            db_aln_start = (
                start + len(unp) - len(alignment.traceback.query.lstrip("-"))
            )
            db_aln_end = db_aln_start + len(
                alignment.traceback.query.rstrip("-")
            )
            seq_aln_start = len(trg) - len(alignment.traceback.ref.lstrip("-"))
            seq_aln_end = seq_aln_start + len(
                alignment.traceback.query.replace("-", "")
            )
            # print("TRG", alignment.traceback.query)
            # print("UNP", alignment.traceback.ref)
            # print(f"TRG {seq_aln_start} {seq_aln_end}")
            return (
                db_aln_start + 1,
                db_aln_end,
                seq_aln_start + 1,
                seq_aln_end,
                alignment.traceback.ref,
            )

        entry = self.get(unp_ac)
        # depositor has provided alignment range, so we only align that region
        db_aln_start, db_aln_end, seq_aln_start, seq_aln_end, aln_unp = aln(
            entry.unp_seq, sequence, start - 1, end
        )
        # print(
        #     f"{entry.entry_version} - start: {start}/ {db_aln_start+1} end: "
        #     + f"{end}/ {db_aln_end} len: {len(sequence)} "
        #     + f"({end-start})"
        # )
        # Check that the alignment fits... make sure the aligned UNP sequence
        # does not contain gaps, gaps in the UNP sequence would mean deleted
        # residues in the target sequence.
        if (
            db_aln_start == start
            and db_aln_end == end
            and aln_unp.find("-") == -1
        ):
            return (
                entry,
                (db_aln_start, db_aln_end),
                (seq_aln_start, seq_aln_end),
            )
        version = entry.entry_version
        while version > 1:
            version -= 1
            entry = self.get(unp_ac, entry_version=version)
            db_aln_start, db_aln_end, seq_aln_start, seq_aln_end, aln_unp = (
                aln(entry.unp_seq, sequence, start - 1, end)
            )
            # print(
            #     f"{version} - start: {start}/ {db_aln_start+1} end: {end}/ "
            #     + f"{db_aln_end} len: {len(sequence)} ({end-start})"
            # )
            if (
                db_aln_start == start
                and db_aln_end == end
                and aln_unp.find("-") == -1
            ):
                return (
                    entry,
                    (db_aln_start, db_aln_end),
                    (seq_aln_start, seq_aln_end),
                )
        # print("FOUND", entry.unp_seq.find(sequence))
        # if we end up here, there was no nice alignment
        # ToDo: use _abort_msg here, once there is a proper Python module
        raise RuntimeError(
            f"Could not find a proper alignment in region {start}-{end} for "
            + f"{unp_ac}."
        )


#  LocalWords:  de strptime UniProtKBEntry
