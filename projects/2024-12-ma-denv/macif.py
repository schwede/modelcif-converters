"""mmCIF editing for ModelArchive depositions.

Supposed to look like gemmi.cif but with some convenience on top. Not meant to
show high-performance but help preparing ModelCIF files for a MA deposition.
Use for 'one time' jobs, not as frequently run tool in a service/ pipeline."""

import gzip

# For whatever reason, 'no-name-in-module' can not be silenced by config atm
# pylint: disable=no-name-in-module
from gemmi import cif

# pylint: enable=no-name-in-module
import gemmi


def _gemmi_quote(value):
    """Quote string values when necessary."""
    if (
        isinstance(value, str)
        and " " in value
        and not (value.startswith("'") and value.endswith("'"))
        and not (value.startswith('"') and value.endswith('"'))
    ):
        return cif.quote(value)
    return value


class MABlock:
    """gemmi.cif wrapper that skips reading/ documents and jumps right into
    gemmi.cif.Block objects. You have all the gemmi.cif.Block functionality
    available plus our own convenience functions on top."""

    def __init__(self, model_data):
        """Initialise a single Block from a file."""
        self.source = model_data
        self.doc = cif.read(model_data)
        self.block = self.doc.sole_block()

        self._targets = None
        self._polymer_targets = None

    def __getattr__(self, name):
        """If an attribute is not found, try self.block before exception."""
        # The catch here is: when asking for self.foo,
        # self.__getattribute__(self, "foo") is executed first. If "foo" is
        # not found in self, self.__getattr__(self, "foo") is called. So here
        # we already know, that "foo" is not there and we can check the
        # original block.
        if hasattr(self.block, name):
            return getattr(self.block, name)
        raise AttributeError(
            f"'{type(self).__name__}' object has no attribute '{name}'"
        )

    @property
    def targets(self):
        """Info about targets."""
        if self._targets is not None:
            return self._targets
        self._targets = {}
        table = self.find("_ma_target_entity.", ["entity_id"])
        for row in table:
            if row["entity_id"] in self._targets:
                raise RuntimeError(
                    f"Target with entity_id '{row['entity_id']}' is duplicated."
                )
            self._targets[row["entity_id"]] = {
                "entity_id": row["entity_id"],  # makes live easier for singles
                "sequence": self.get_sequence(row["entity_id"]),
            }
        table = self.find("_entity.", ["id", "type"])
        for row in table:
            self._targets[row["id"]]["type"] = row["type"]

        return self._targets

    @property
    def polymer_targets(self):
        """Only targets of entity type 'polymer'."""
        if self._polymer_targets is not None:
            return self._polymer_targets
        self._polymer_targets = []
        for target in self.targets.values():
            if target["type"] == "polymer":
                self._polymer_targets.append(target)

        return self._polymer_targets

    def find(self, name, columns):
        """Get a table with defined colums. Throws an exception if table is not
        found."""
        table = self.block.find(name, columns)
        if len(table) == 0:
            raise RuntimeError(
                f"""Table '{name}' with columns '{"', '".join(columns)}' """
                + "not found."
            )

        return table

    def get_sequence(self, entity):
        """Get the sequence of a 'polymer' entity. `entity` is the numeric ID
        of the entity.
        Reading sequences is inefficient atm, it reads the whole table for
        every sequence w/o any caching."""
        table = self.find("_entity_poly_seq.", ["entity_id", "num", "mon_id"])
        sequence = ""
        num = 0
        for row in table:
            if row["entity_id"] != entity:
                continue
            num += 1
            assert int(row["num"]) == num
            sequence += gemmi.find_tabulated_residue(
                row["mon_id"]
            ).one_letter_code

        return sequence

    def write_file(self, filename, compress=False, style=cif.Style.Simple):
        """Write ModelCIF file to disk, compress upon request.
        Will compress anyways if file ends with '.gz'."""
        if compress or filename.endswith(".gz"):
            if not filename.endswith(".gz"):
                filename += ".gz"
            with gzip.open(filename, mode="wt", compresslevel=9) as gfh:
                gfh.write(self.doc.as_string(style))
        else:
            self.doc.write_file(filename, style)

    def add_to_category(self, category, match=None, **kwargs):
        """Add item values to a category.
        Keyword arguments are reserved for item names."""
        if category[0] != "_":
            category = "_" + category
        if category[-1] != ".":
            category += "."
        items = list(kwargs.keys())
        row = None
        if match is not None:
            table = self.find(category, items + [match[0]])
            for row in table:
                if row[match[0]] == match[1]:
                    break
            if row is None:
                raise RuntimeError(
                    f"No item {match[0]}=={match[1]} found in category "
                    + f"{category}."
                )
        else:
            table = self.find(category, items)
            assert len(table) == 1
            row = table[0]
        for itm, val in kwargs.items():
            if row[itm] not in [".", "?"]:
                print(
                    f"    replacing '{cif.as_string(row[itm])}' with "
                    + f"'{val}' ({itm})"
                )
            row[itm] = _gemmi_quote(val)

    def add_category(self, category, after=None, **kwargs):
        """Add a new category to the block with only 1 set of values, 1 row
        thinking of categories as tables. kwargs are reserved for item/ value
        pairs. after is a special keyword parameter to locate the new category
        inside the block."""
        if not category.startswith("_"):
            category = "_" + category
        # handle quoting
        for values in kwargs.values():
            for i, val in enumerate(values):
                values[i] = _gemmi_quote(val)
        self.block.set_mmcif_category(category, kwargs, raw=True)

        if after is None:
            return
        if not after.startswith("_"):
            after = "_" + after
        if not after.endswith("."):
            after += "."
        table = self.block.find_mmcif_category(after)
        idx = self.block.get_index(table.tags[-1])
        # be careful with move_item: loops are 1 item and move as a whole,
        # single line values move per item/ value par.
        self.block.move_item(-1, idx + 1)
