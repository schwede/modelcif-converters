# Projects

This directory tree contains tools/ code from past modelling projects converted to [ModelCIF](https://mmcif.wwpdb.org/dictionaries/mmcif_ma.dic/Index/) for [MA](https://www.modelarchive.org).

Each project should come with a small README explaining the modelling project. This will help you checking if your project is of same kind. The project folders are named as [YYYY-MM]-[MA-ID] according to the year and month when the conversion was done and the ID in ModelArchive.

The [`docker`](projects/docker/) directory does not host a modelling project. It keeps the set up of a [Docker](https://www.docker.com) image that can be used to run the converter tools from the various projects.

<!--
Adding projects:
- file permissions for the translation script (755) so it can be executed by any
  user
- mention .dockerignore in projects README.md, like when adding projects consider..., add a reference to it, in the docker/README.md

-->

<!--  LocalWords:  README dockerignore
 -->
